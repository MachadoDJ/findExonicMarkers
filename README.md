i. AVAILABILITY

All programs herein come with ABSOLUTELY NO WARRANTY and are available at
www.ib.usp.br/grant/anfibios/researchSoftware.html.

The programs may be freely used, modified, and shared under the GNU General
Public License version 3.0 (GPL-3.0, http://opensource.org/licenses/GPL-3.0).

See LICENSE.txt for complete information about terms and conditions.

Programs are also available at also available at GitLab:
https://gitlab.com/MachadoDJ/.

Also, check the wiki page that are available at GitLab.

---

ii. CONTACT

* Author: Denis Jacob Machado
* Homepage: http://about.me/machadodj
* Email: machadodj<at>usp.br
* Our lab's website: http://www.ib.usp.br/grant/anfibios

---

iii. SUMMARY

The programs herein are part of a pipeline to compare two anuran genomes and
extract primers for intra and inter-exon markers.

Primer estimation follows the following criteria:

* Flanking unmasked DNA: >=24bp each end.
* Target size: <=600bp.
* TM: 57—63ºC, optimal of 60ºC.
* Maximum difference in TM = 2ºC.
* GC content: 20—80%, optimal of 50%.
* Primer size: 18—27bp, optimal of 20bp.

---

1. DEPENDENCIES

This program requires a working installation of Primer3 (tested with libprimer3
release 2.3.7).

This program was written for Pyhton v3.5+.

This program was written in MacOS, and should work in any Unix/ Linux
distribution.

The programs herein are part of a pipeline to extract intra and inter-exonic
markers. This pipeline also requires CD-HIT (http://weizhongli-lab.org/cd-hit/).

Example command lines use two draft genomes of the frogs Phyllobates terribilis
("pterribilis") and Scaphiopus holbrookii ("sholbrookii").

---

3. CLUSTERING EXONS

The output from CD-HIT only prints up to 20 characters of the sequence ID.
Therefore, I replaced all sequence IDs for an alphanumeric code using the
homemade Python script named fa_encrypt.py. UUID of 16 characters are given to
each sequence, so they can all be identified in the output of CH-HIT (I mean the
log files, full descriptors are used on the multifasta output files).

```
$ python3 fa_encrupt.py -f <fasta file> -m 0 -o <output prefix>

$ python3 fa_encrupt.py -f <fasta file> -s <synonyms file> -m 1 \
    -o <output prefix>
```

We need to cluster exons from the same species at different identity thresholds
using CD-HIT-EST to extract representative sequences at each identity level:

```
$ cd-hit-est -i renamed_pt_pterribilis_exons.fasta \
    -o pterribilis_cluster100.fasta -c 1.0 -n 10 -M 512000 \
    -T 64 -B 0 -g 1

$ cd-hit-est -i renamed_pt_pterribilis_exons.fasta \
    -o pterribilis_cluster95.fasta -c 0.95 -n 8 -M 512000 \
    -T 64 -B 0 -g 1

$ cd-hit-est -i renamed_pt_pterribilis_exons.fasta \
    -o pterribilis_cluster80.fasta -c 0.80 -n 5 -M 512000 \
    -T 64 -B 0 -g 1

$ cd-hit-est -i renamed_pt_sholbrookii_exons.fasta \
    -o sholbrookii_cluster100.fasta -c 1.0 -n 10 -M 512000 \
    -T 64 -B 0 -g 1

$ cd-hit-est -i renamed_pt_sholbrookii_exons.fasta \
    -o sholbrookii_cluster95.fasta -c 0.95 -n 8 -M 512000 \
    -T 64 -B 0 -g 1

$ cd-hit-est -i renamed_pt_sholbrookii_exons.fasta \
    -o sholbrookii_cluster80.fasta -c 0.80 -n 5 -M 512000 \
    -T 64 -B 0 -g 1
```

Then, different combinations of order os species (P. terribiis vs.
S. holbrookii, or vice-versa) and identity thresholds were used to cluster exons
between species with CD-HIT-EST-2D:

```
$ cd-hit-est-2d -i renamed_pt_pterribilis_exons.fasta \
    -i2 renamed_sh_sholbrookii_exons.fasta -o pt-sh-100.fasta \
    -c 1.0 -n 10  -M 512000 -T 64 -B 0 -g 1

$ cd-hit-est-2d -i renamed_pt_pterribilis_exons.fasta \
    -i2 renamed_sh_sholbrookii_exons.fasta -o pt-sh-95.fasta \
    -c 0.95 -n 8  -M 512000 -T 64 -B 0 -g 1

$ cd-hit-est-2d -i renamed_pt_pterribilis_exons.fasta 
    -i2 renamed_sh_sholbrookii_exons.fasta -o pt-sh-80.fasta 
    -c 0.80 -n 5  -M 512000 -T 64 -B 0 -g 1

$ cd-hit-est-2d -i renamed_sh_sholbrookii_exons.fasta \
    -i2 renamed_pt_pterribilis_exons.fasta -o pt-sh-100.fasta \
    -c 1.0 -n 10  -M 512000 -T 64 -B 0 -g 1

$ cd-hit-est-2d -i renamed_sh_sholbrookii_exons.fasta \
    -i2 renamed_pt_pterribilis_exons.fasta -o pt-sh-95.fasta \
    -c 0.95 -n 8  -M 512000 -T 64 -B 0 -g 1

$ cd-hit-est-2d -i renamed_sh_sholbrookii_exons.fasta 
    -i2 renamed_pt_pterribilis_exons.fasta -o pt-sh-80.fasta 
    -c 0.80 -n 5  -M 512000 -T 64 -B 0 -g 1
```

We parse CD-HIT results using two independent homemade Python scripts,
parseCDHITEST.py and parseCDHITEST2.py. With this strategy, we can merge
merging clusters within each species as well as between them, and extract the
better representative sequence from each cluster using CD-HIT parameters. This
strategy follows a conservative rationale that ignores some valid exon pairs
that are homologous to reduce the chance of comparing two non-homologous exons.

```
$ python3 parseCDHITEST.py \
    --source1 <multifasta with renamed exons from species 1> \
    --source2 <multifasta with renamed exons from species 2> \
    --representatives1 <.clstr for sp. 1 from cd-hit-est> \
    --representatives2 <.clstr for sp. 2 from cd-hit-est> \
    --assembly1 <multifasta with scaffolds from species 1> \
    --assembly2 <multifasta with scaffolds from species 2>
```

he program parseCDHITEST.py will return multifasta files with the
representatives from each cluster. The description of each entry include the
cluster identification number and its size (i.e., how many exons are included in
the same clusters). The program parseCDHITEST2.py will read these representative
sequences along with the synonym files (in tab-separated format) and original
scaffolds (in multifasta format) to calculate statistics and call PRIMER3.

```
$ python3 parseCDHITEST2.py \
    --input1 <representatives for sp. 1 from parseCDHITEST.py> \
    --input2 <representatives for sp. 2 from parseCDHITEST.py> \
    --synonym1 <.tsv file with synonyms for exons in sp. 1> \
    --synomym2 <.tsv file with synonyms for exons in sp. 2> \
    --assembly1 <multifasta with scaffolds from species 1> \
    --assembly2 <multifasta with scaffolds from species 2>
```

---

4. PRIMER EXTRACTION

The program findPrimersForGenes.py takes the configuration files written by
parseCDHITEST.py and uses them to run PRIMER3 and propose the primers for intra
and inter-exon primers.

```
$ python3 findPrimersForGenes.py \
    --config <Path to the directory of configuration files> \
    --output <Output prefix> \
    --primer3 <Path to the primer3_core executable> \
    -t <.tsv with sequence ID, target start, end, and identity>
```

Note that findPrimersForGenes.py requires a working installation of Primer3
(tested with libprimer3 release 2.3.7).

---

5. CITE

Machado,D.J. (2018) Comparative Genomics and the Evolution of Amphibian Chemical
Defense.

Ph.D. dissertation. Universidade de São Paulo, Programa de Pós-Graduação em
Bioinformática.