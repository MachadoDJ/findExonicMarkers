#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# parseCDHITEST2.py

# Read the representative sequences exported by parseCDHITEST and report a few stats as
# well as the input for primer design

################################
# Import modules and libraries #
################################

from Bio import pairwise2
import argparse, re, sys

#############
# Arguments #
#############

parser=argparse.ArgumentParser()
parser.add_argument("--input1", help="Multifasta file with representatives from species 1", type=str, required=True)
parser.add_argument("--input2", help="Multifasta file with representatives from species 2", type=str, required=True)
parser.add_argument("--synonym1", help="Tab-separated synonyms file 1", type=str, required=True)
parser.add_argument("--synonym2", help="Tab-separated synonyms file 2", type=str, required=True)
parser.add_argument("--assembly1", help="Multifasta file with scaffolds from species 1", type=str, required=True)
parser.add_argument("--assembly2", help="Multifasta file with scaffolds from species 2", type=str, required=True)
args=parser.parse_args()

####################
# Global variables #
####################

file1      = args.input1    # File 1, species 1, multifasta
file2      = args.input2    # File 2, species 2, multifasta
syns1      = args.synonym1  # File 1, synonyms for species 1
syns2      = args.synonym2  # File 2, synonyms for species 2
assembly_1 = args.assembly1 # All scaffolds from species 1, multifasta
assembly_2 = args.assembly2 # All scaffolds from species 2, multifasta

############################
# Define parsing functions #
############################

def read_representatives(file, x):
	dic = {} # dic={uuid:[cluster,members,length,%GC,%non-ACGT,%soft-masked,seq]}
	handle = open(file, "r")
	for head, seq in re.compile(">([^\n]+)\n([^>]+)", re.MULTILINE|re.DOTALL).findall(handle.read()):
		id, cluster, size = re.compile("^([^\s]+)\s+cluster_id=(\d+)\s+size=(\d+)").findall(head)[0]
		seq = re.sub("[\s\n\r]", "", seq)
		if (id != "NO_MATCH"):
			length = len(seq)
			if (length < x):
				pass
			else:
				try:
					gc = (sum([1 for base in seq if base in ["C", "G", "c", "g"]])) / (length-sum([1 for base in seq if base not in ["A", "C", "G", "T", "a", "c", "g", "t"]]))
				except:
					sys.stderr.write("¡ Warninig: no usable characters in {}, %GC = 0.0 !\n".format(id))
					gc = 0
				amb = (sum([1 for base in seq if base not in ["A" ,"C", "G", "T", "a", "c", "g", "t"]])) / (length)
				masked = (sum([1 for base in seq if base.islower()])) / (length)
				dic[id] = [int(cluster), int(size), int(length), float(gc), float(amb), float(masked), str(seq)]
	handle.close()
	return dic

def read_assembly(file):
	sys.stdout.write("-- Parsing multifasta file {}\n".format(file))
	dic    = {}
	handle = open(file, "r")
	for line in handle.readlines():
			line = line.strip()
			if(not line):
				continue
			if(line.startswith(">")):
					description          = re.compile(">\s*([^\s]+)").findall(line)[0]
					active_sequence_name = description
					if active_sequence_name not in dic:
						dic[active_sequence_name] = ""
					continue
			dic[active_sequence_name] += line
	handle.close()
	sys.stdout.write("-- Done parsing multifasta file {}\n".format(file))
	return dic

def read_synonyms(file):
	sys.stdout.write("-- Reading synonyms file {}\n".format(file))
	dic    = {} # dic={uuid : [original id, start, end]}
	handle = open(file,"r")
	for line in handle.readlines():
		line = line.strip()
		if(line):
			try:
				uuid, description = line.split("\t")
			except:
				pass
			else:
				id, x, y = re.compile("(.+)\s+range=(\d+):(\d+)").findall(description)[0]
				x = int(x)
				y = int(y)
				start = min(x,y)
				end = max(x,y)
				if(not uuid in dic):
					dic[uuid] = [id, start, end]
				else:
					sys.stderr.write("¡ ERROR 005 !\n")
					exit()
	handle.close()
	return dic

##############################
# Define filtering functions #
##############################

def filter_by_length(dic, x, y):
	for id in [id for id in dic]:
		v = dic[id][2]
		if (v < x or v > y):
			del dic[id]
	return dic

def filter_by_gc(dic, x, y):
	for id in [id for id in dic]:
		v = dic[id][3]
		if (v < x or v > y):
			del dic[id]
	return dic

def filter_by_amb(dic, x):
	for id in [id for id in dic]:
		v = dic[id][4]
		if(v > x):
			del dic[id]
	return dic

def filter_by_mask(dic, x):
	for id in [id for id in dic]:
		v = dic[id][5]
		if (v > x):
			del dic[id]
	return dic

def filter_by_length_diff(dic1, dic2, x):
	select = {} # select={cluster:[length 1, length 2]}
	for id in dic1:
		cluster = dic1[id][0]
		length  = dic1[id][2]
		if(cluster in select):
			sys.stderr.write("¡ ERROR 001 !\n")
			exit()
		else:
			select[cluster] = [length]
	for id in dic2:
		cluster = dic2[id][0]
		length  = dic2[id][2]
		if (cluster in select):
			if (len(select[cluster]) != 1):
				sys.stderr.write("¡ ERROR 002 !\n")
				exit()
			else:
				select[cluster] += [length]
	for cluster in [cluster for cluster in select]:
		if (len(select[cluster]) == 2):
			a = select[cluster][0]
			b = select[cluster][1]
			v = abs(a - b) / max(a, b)
			if (v > x):
				del select[cluster]
		elif(len(select[cluster])==1):
			del select[cluster]
		else:
			sys.stderr.write("¡ ERROR 003 !\n")
			exit()
	select = [cluster for cluster in select]
	for id in [id for id in dic1]:
		if (not dic1[id][0] in select):
			del dic1[id]
	for id in [id for id in dic2]:
		if (not dic2[id][0] in select):
			del dic2[id]
	return dic1, dic2

def filter_by_matches(dic1, dic2,x):
	select = set([dic1[id][0] for id in dic1 if 1 <= dic1[id][1] <=x]) & set([dic2[id][0] for id in dic2 if 1 <= dic2[id][1] <=x])
	for id in [id for id in dic1]:
		if (not dic1[id][0] in select):
			del dic1[id]
	for id in [id for id in dic2]:
		if (not dic2[id][0] in select):
			del dic2[id]
	return dic1, dic2

###########################
# Define marker functions #
###########################

def retrieve_homologs(dic1, dic2, x):
	homologs = {} # homologs = {cluster : [ [uuid1, seq1] , [uuid2, seq2] ] , ...}
	for id in dic1:
		cluster = dic1[id][0]
		seq     = dic1[id][6]
		if (not cluster in homologs):
			homologs[cluster] = [[id,seq]]
		else:
			sys.stderr.write("¡ ERROR: unexpected number of cluster data found during homologs retrieval!\n")
			exit()
	for id in dic2:
		cluster = dic2[id][0]
		seq     = dic2[id][6]
		if (cluster in homologs):
			if (len(homologs[cluster])==1):
				homologs[cluster]+=[[id,seq]]
	record_identity = []
	for cluster in [cluster for cluster in sorted(homologs)]:
		if (len(homologs[cluster]) != 2):
			del homologs[cluster]
	for cluster in [cluster for cluster in sorted(homologs)]:
		seq1           = homologs[cluster][0][1]
		seq2           = homologs[cluster][1][1]
		alignments     = pairwise2.align.globalms(seq1, seq2, 5, -4, -3, -0.1) # alignment=[(align1,align2,score,n,length),...]
		score          = alignments[0][2]
		alignments_rev = pairwise2.align.globalms(seq1, reverse_complement(seq2), 5, -4, -3, -0.1) # alignment=[(align1,align2,score,n,length),...]
		score_rev      = alignments_rev[0][2]
		if (score_rev > score):
			alignments              = alignments_rev
			seq2                    = reverse_complement(seq2)
			homologs[cluster][1][1] = seq2
		scores = []
		lens   = []
		for a in alignments:
			scores.append(a[2])
			lens.append(a[4])
		scores = set(scores)
		lens   = set(lens)
		if   (len(scores) == 1) and (len(lens) == 1):
			pass
		elif (len(scores) == 1) and (len(lens) != 1):
			alignments = [a for a in alignments if a[4] == min(lens)]
		else:
			sys.stderr.write("¡ ERROR 004 !\n")
			exit()
		identity = 0.0
		for i in range(0, len(alignments)):
			identity += sum([1 for a, b in zip(alignments[i][0], alignments[i][1]) if a == b]) / float(alignments[i][4])
		identity         = 100.0 * (identity/len(alignments))
		record_identity += [identity]
		if (identity <= x):
			del homologs[cluster]
		sys.stdout.write("<< Cluster {:,} (identity={}%) - alignment:\n{}>>\n".format(cluster, identity, pairwise2.format_alignment(*alignments[0])))
	message  = "\n-- Homolog pairs = {:,}".format(len(record_identity))
	message += " (sequence identity {0:.2f}".format(min(record_identity))
	message += "-{0:.2f}%)\n".format(max(record_identity))
	sys.stdout.write("{}".format(message))
	return homologs

def find_intra_exon_markers(homologs, x, y, z): # x = min_primer_length, y = min_target_length, z = max_target_length
	sys.stdout.write("-- Examining clusters for candidate markers\n")
	data4primer = {}
	for cluster in [cluster for cluster in homologs]:
		id1 = homologs[cluster][0][0]
		sq1 = homologs[cluster][0][1]
		id2 = homologs[cluster][1][0]
		sq2 = homologs[cluster][1][1]
		ln1 = len(sq1)
		ln2 = len(sq2)
		if (min(ln1, ln2) >= (2 * x + y)):
			# Part 1: find stable starting sequence
			sq1, sq2, ln1, ln2, word = extract_identical_head(sq1, sq2, ln1, ln2, x, y)
			if (len(word) != 0) and (min(ln1, ln2) >= x+y):
				# Part 2: find stable ending sequence
				sq1, sq2, ln1, ln2, drow = extract_identical_tail(sq1, sq2, ln1, ln2, x, y)
				if (len(drow) != 0) and (min(ln1, ln2) > y):
					# Part 3: Calculate variability of the target
					identity = calculate_identity(sq1, sq2)
					if (identity <= 0.95) and (len(sq1) <= z):
						data4primer[id1] = {"cluster" : cluster, "head" : word, "tail" : drow, "target" : sq1, "identity" : identity}
	sys.stdout.write("-- Selected targets = {} (of {} homolog pairs)\n".format(len(data4primer), len(homologs)))
	sys.stdout.write("//\n# Intra-exon markers\n# ID\tSequence\tTarget start\tTarget end\tIdentity\n")
	for id in data4primer:
		head     = data4primer[id]["head"]
		target   = data4primer[id]["target"]
		tail     = data4primer[id]["tail"]
		identity = "{0:.2f}%".format(100.0*data4primer[id]["identity"])
		sys.stdout.write("{}\t{}{}{}\t{}\t{}\t{}\n".format(id, head, target, tail, len(head) +1, len(head) + len(target), identity))
	sys.stdout.write("//\n")
	return

def find_inter_exon_markers(homologs, syns1, syns2, asse1, asse2, x, y): # x = max_intron_length, y = min_primer_length
	sys.stdout.write("-- Examining scaffolds for candidate markers\n")
	genes_1          = {} # genes = {original id : [[start, end, cluster, uuid], ...], ...}
	selected         = {} # selected = {candidate number : [original id 1, sequence, target start, target end, identity]}
	candidate_number = 0
	###########################
	# Create genes dictionary #
	###########################
	for cluster in [cluster for cluster in sorted(homologs)]:
		uuid_1        = homologs[cluster][0][0]
		exon_1        = homologs[cluster][0][1]
		# uuid_2        = homologs[cluster][1][0]
		exon_2        = homologs[cluster][1][1]
		original_id_1 = syns1[uuid_1][0]
		scaffold_1    = asse1[original_id_1]
		start_1       = scaffold_1.find(exon_1)
		if (start_1 == -1):
			exon_1  = reverse_complement(exon_1)
			exon_2  = reverse_complement(exon_2)
			start_1 = scaffold_1.find(exon_1)
			if(start_1 == -1):
				sys.stderr.write("¡ ERROR 007 !\n")
				exit()
			else:
				end_1                   = start_1 + len(exon_1)
				homologs[cluster][0][1] = exon_1
				homologs[cluster][1][1] = exon_2
		else:
			end_1 = start_1 + len(exon_1)
		if (original_id_1 in genes_1):
			genes_1[original_id_1] += [[start_1, end_1, cluster, uuid_1]]
		else:
			genes_1[original_id_1]  = [[start_1, end_1, cluster, uuid_1]]
	sys.stdout.write("-- Gene dictionary acquired\n")
	#########################
	# Sort genes dictionary #
	#########################
	for original_id_1 in genes_1:
		genes_1[original_id_1] = sorted(genes_1[original_id_1])
	sys.stdout.write("-- Gene dictionary sorted\n")
	###########
	# Compare #
	###########
	for original_id_1 in [i for i in sorted(genes_1)]:
		sys.stdout.write("-- Scanning sequence {} for candidate {}\n".format(original_id_1, candidate_number))
		series = genes_1[original_id_1]
		while True:
			new_pair = series[0:2]
			series   = series[1:]
			if (len(new_pair) != 2):
				break
			else:
				scaffold_1       = asse1[original_id_1]
				cluster_1        = new_pair[0][2]
				cluster_2        = new_pair[1][2]
				uuid_s2c1        = homologs[cluster_1][1][0]
				uuid_s2c2        = homologs[cluster_2][1][0]
				exon_s1c1        = homologs[cluster_1][0][1]
				exon_s2c1        = homologs[cluster_1][1][1]
				exon_s1c2        = homologs[cluster_2][0][1]
				exon_s2c2        = homologs[cluster_2][1][1]
				original_id_s2c1 = syns2[uuid_s2c1][0]
				original_id_s2c2 = syns2[uuid_s2c2][0]
				if (original_id_s2c1 == original_id_s2c2): # Check to see if matching exons are on the same scaffold
					##############################
					# Fragment 1 from scaffold 1 #
					##############################
					start_s1c1 = scaffold_1.find(exon_s1c1)
					start_s1c2 = scaffold_1.find(exon_s1c2)
					if (start_s1c1 == -1):
						sys.stderr.write("¡ ERROR 010 !\n")
						exit()
					if (start_s1c2 == -1):
						sys.stderr.write("¡ ERROR 011 !\n")
						exit()
					if (start_s1c1 > start_s1c2):
						sys.stderr.write("¡ ERROR 012 !\n")
						exit()
					end_s1c1                  = start_s1c1 + len(exon_s1c1)
					end_s1c2                  = start_s1c2 + len(exon_s1c2)
					fragment_1                = scaffold_1[start_s1c1 : end_s1c2]
					fragmt_sta_1              = 0
					fragmt_end_1              = len(fragment_1)
					target_sta_1              = len(exon_s1c1)
					target_end_1              = fragmt_end_1 - len(exon_s1c2)
					exon_s1c1                 = fragment_1[:target_sta_1]
					homologs[cluster_1][0][1] = exon_s1c1
					exon_s1c2                 = fragment_1[target_end_1:]
					homologs[cluster_2][0][1] = exon_s1c2
					##############################
					# Fragment 2 from scaffold 2 #
					##############################
					original_id_2  = original_id_s2c1
					scaffold_2     = asse2[original_id_2]
					start_s2c1     = scaffold_2.find(exon_s2c1)
					if (start_s2c1 == -1):
						exon_s2c1  = reverse_complement(exon_s2c1)
						start_s2c1 = scaffold_2.find(exon_s2c1)
						if(start_s2c1 == -1):
							sys.stderr.write("¡ ERROR 008 !\n")
							exit()
					end_s2c1 = start_s2c1 + len(exon_s2c1)
					start_s2c2 = scaffold_2.find(exon_s2c2)
					if (start_s2c2 == -1): # A few hits might fall here, normaly when sequence identity among exons is low and it is hard to be sure of the orientation
						exon_s2c2 = reverse_complement(exon_s2c2)
						start_s2c2 = scaffold_2.find(exon_s2c2)
						if (start_s2c2 == -1):
							sys.stderr.write("¡ ERROR 009 !\n")
							exit()
					end_s2c2 = start_s2c2 + len(exon_s2c2)
					fragment_2 = scaffold_2[min(start_s2c1, end_s2c1, start_s2c2, end_s2c2): max(start_s2c1, end_s2c1, start_s2c2, end_s2c2)]
					if(start_s2c1 > start_s2c2):
						fragment_2 = reverse_complement(fragment_2)
					fragmt_sta_2              = 0
					fragmt_end_2              = len(fragment_2)
					target_sta_2              = len(exon_s2c1)
					target_end_2              = fragmt_end_2 - len(exon_s2c2)
					exon_s2c1                 = fragment_2[ : target_sta_2]
					homologs[cluster_1][1][1] = exon_s2c1
					exon_s2c2                 = fragment_2[target_end_2 : ]
					homologs[cluster_2][1][1] = exon_s2c2
					#################################################
					# Calculate identity and find candidate markers #
					#################################################
					sq1, sq2, ln1, ln2, tail = extract_identical_head(exon_s1c2, exon_s2c2, len(exon_s1c2), len(exon_s2c2), y, 0)
					sq1, sq2, ln1, ln2, head = extract_identical_tail(exon_s1c1, exon_s2c1, len(exon_s1c1), len(exon_s2c1), y, 0)
					if (len(head)>=y) and (len(tail)>=y):
						start_1                     = fragment_1.find(head)
						start_2                     = fragment_2.find(head)
						end_1                       = fragment_1.find(tail)
						end_2                       = fragment_2.find(tail)
						target_start_1              = start_1 + len(head)
						target_start_2              = start_2 + len(head)
						target_end_1                = end_1
						target_end_2                = end_2
						target_1                    = fragment_1[target_start_1 : target_end_1]
						target_2                    = fragment_2[target_start_2 : target_end_2]
						target_expected_identity    = calculate_identity(target_1, target_2)
						target_expected_identity    = 100.0 * target_expected_identity
						avg_intron_size             = (len(target_1) + len(target_2)) / 2
						if (avg_intron_size <= x) and (target_expected_identity < 95.0):
							selected[candidate_number] = [original_id_1, fragment_1, target_start_1, target_end_1, target_expected_identity]
							candidate_number          += 1
	sys.stdout.write("-- {} candidate markes acquired\n".format(candidate_number))
	sys.stdout.write("//\n# Inter-exon markers\n# ID\tSequence\tTarget start\tTarget end\tIdentity\n")
	all_identities = []
	all_lengths    = []
	for candidate_number in sorted(selected):
		name     = selected[candidate_number][0]
		seq      = selected[candidate_number][1]
		start    = selected[candidate_number][2]
		end      = selected[candidate_number][3]
		identity = selected[candidate_number][4]
		all_identities.append(identity)
		all_lengths.append(end - start)
		sys.stdout.write("{}\t{}\t{}\t{}\t{}\n".format(name, seq, start, end, identity))
	sys.stdout.write("# N = {}\n".format(candidate_number))
	sys.stdout.write("# Expected identity variation = {:.2f}% ({:.2f}% : {:.2f}%)\n".format(sum(all_identities) / len(all_identities), min(all_identities), max(all_identities)))
	sys.stdout.write("# Target length variation = {} ({} : {})\n//\n".format(sum(all_lengths) / len(all_lengths), min(all_lengths), max(all_lengths)))
	return

##############################
# Define auxiliary functions #
##############################

def reverse_complement(seq):
	complement={"A":"T","T":"A","U":"A","G":"C","C":"G","Y":"R","R":"Y","S":"S","W":"W","K":"M","M":"K","B":"V","D":"H","H":"D","V":"B","N":"N","a":"t","t":"a","u":"a","g":"c","c":"g","y":"r","r":"y","s":"s","w":"w","k":"m","m":"k","b":"v","d":"h","h":"d","v":"b","n":"n"}
	bases=list(seq)
	bases=reversed([complement.get(base,base) for base in bases])
	bases=''.join(bases)
	return bases

def stats(dic1,dic2,message):
	sys.stdout.write("{}\n".format(message))
	clusters=set([dic1[id][0] for id in dic1])|set([dic2[id][0] for id in dic2])
	sys.stdout.write("-- No. of clusters = {:,}\n".format(len(clusters)))
	size11=set([dic1[id][0] for id in dic1 if dic1[id][1]==1])&set([dic2[id][0] for id in dic2 if dic2[id][1]==1])
	sys.stdout.write("-- Clusters with 1 to 1 matches = {:,}\n".format(len(size11)))
	return

def calculate_identity(seq1, seq2):
	alignments = pairwise2.align.globalms(seq1, seq2, 5, -4, -3, -0.1) # alignment=[(align1,align2,score,n,length),...]
	if(alignments):
		scores = []
		lens   = []
		for a in alignments:
			scores.append(a[2])
			lens.append(a[4])
		scores = set(scores)
		lens   = set(lens)
		if (len(scores) == 1) and (len(lens) == 1):
			pass
		elif (len(scores) == 1) and (len(lens) != 1):
			alignments = [a for a in alignments if a[4] == min(lens)]
		else:
			sys.stderr.write("¡ ERROR 006 !\n***\nSeq. 1 = {}\nSeq. 2 = {}\nAlignments = {}\n***\n".format(seq1, seq2, alignments))
			exit()
		identity = 0.0
		for i in range(0, len(alignments)):
			identity += sum([1 for a, b in zip(alignments[i][0], alignments[i][1]) if a == b]) / float(alignments[i][4])
		identity = identity / len(alignments)
	else:
		sys.stderr.write("¡ ERROR 006 !\n***\nSeq. 1 = {}\nSeq. 2 = {}\nAlignments = {}\n***\n".format(seq1,seq2,alignments))
		exit()
	return identity

def extract_identical_head(sq1,sq2,ln1,ln2,x,y):
	count=0
	while (x+count)<=(ln1-x-y):
		word=sq1[count:x+count]
		f=sq2.find(word)
		if(f!=-1 and f<=ln2-x-y):
			sq1=sq1[count:]
			sq2=sq2[f:]
			ln1=len(sq1)
			ln2=len(sq2)
			for i in range(x,min(ln1,ln2)):
				b1=sq1[i]
				b2=sq2[i]
				if(b1==b2):
					word+=b1
				else:
					break
			sq1=sq1[len(word):]
			sq2=sq2[len(word):]
			ln1=len(sq1)
			ln2=len(sq2)
			break
		count+=1
	if(f==-1):
		word=""
	return sq1,sq2,ln1,ln2,word

def extract_identical_tail(sq1,sq2,ln1,ln2,x,y):
	sq1=sq1[::-1]
	sq2=sq2[::-1]
	count=0
	while (x+count)<=(ln1-y):
		word=sq1[count:x+count]
		f=sq2.find(word)
		if(f!=-1 and f<=ln2-y):
			sq1=sq1[count:]
			sq2=sq2[f:]
			ln1=len(sq1)
			ln2=len(sq2)
			for i in range(x,min(ln1,ln2)):
				b1=sq1[i]
				b2=sq2[i]
				if(b1==b2):
					word+=b1
				else:
					break
			sq1=sq1[len(word):]
			sq2=sq2[len(word):]
			ln1=len(sq1)
			ln2=len(sq2)
			break
		count+=1
	if(f==-1):
		drow=""
	else:
		sq1=sq1[::-1]
		sq2=sq2[::-1]
		drow=word[::-1]
	return sq1,sq2,ln1,ln2,drow

#####################
# Execute functions #
#####################

sys.stdout.write("// Reading scaffold data and synonym files //\n")
asse1 = read_assembly(assembly_1) # Parse scaffolds file 1, dic = {original id : seq}
asse2 = read_assembly(assembly_2) # Parse scaffolds file 2, dic = {original id : seq}
syns1 = read_synonyms(syns1)      # Parse synonyms file 1, dic={uuid : [original id, start, end]}
syns2 = read_synonyms(syns2)      # Parse synonyms file 2, dic={uuid : [original id, start, end]}

# PART 1: Intra-exon markers

min_primer_length = 17 # 'Good' primers are often 18 to 24 bp in length
min_exon_length   = 5 * min_primer_length # In humans, 80% of exons are < 200 bp in length
min_target_length = 3 * min_primer_length
max_target_length = 600
max_intron_length = 600
min_gc            = 0.2
max_gc            = 0.8
max_ambiguities   = 0.01
max_masks         = 0.01
max_length_diff   = 0.5
max_cluster_size  = 10
min_seq_id        = 0.3

sys.stdout.write("// PART 1: Intra-exon markers //\n")
dic1       = read_representatives(file1, min_exon_length)
dic2       = read_representatives(file2, min_exon_length)
stats(dic1, dic2, "[ Original ]")
# dic1       = filter_by_length(dic1, min_exon_length, max_target_length + 10 * min_primer_length)
# dic2       = filter_by_length(dic2, min_exon_length, max_target_length + 10 * min_primer_length)
# stats(dic1,dic2,"[ Filter by length ]")
dic1       = filter_by_gc(dic1, min_gc, max_gc) # Allow exons with 0.2 < %GC < 0.8
dic2       = filter_by_gc(dic2, min_gc, max_gc) # Allow exons with 0.2 < %GC < 0.8
stats(dic1, dic2, "[ Filter by %GC ]")
dic1       = filter_by_amb(dic1, max_ambiguities) # Allow up to 0.01 ambiguities
dic2       = filter_by_amb(dic2, max_ambiguities) # Allow up to 0.01 ambiguities
stats(dic1, dic2, "[ Filter by ambiguity content ]")
dic1       = filter_by_mask(dic1, max_masks) # Allow up to 0.01 soft masked bases
dic2       = filter_by_mask(dic2, max_masks) # Allow up to 0.01 soft masked bases
stats(dic1, dic2, "[ Filter by soft-masked bases ]")
# dic1, dic2 = filter_by_matches(dic1, dic2, max_cluster_size) # Allow clusters within the specified size
# stats(dic1,dic2,"[ Filter by cluster size ]")
dic1, dic2 = filter_by_length_diff(dic1, dic2, max_length_diff) # Maximum allowed length difference between members of the same cluster of 0.1
stats(dic1, dic2, "[ Filter by length differences ]")
homologs   = retrieve_homologs(dic1, dic2, min_seq_id) # Retrieve all sequence pairs with sequence identity greater than 30%, homologs = {cluster : [[uuid1, seq1], [uuid2, seq2]]}
find_intra_exon_markers(homologs, min_primer_length, min_target_length, max_target_length) # Find exons with variable intermediary sequences (at least 80bp) flanked by more stable sequences (at least 17bp)

# PART 2: Inter-exon markers

sys.stdout.write("// PART 2: Inter-exon markers //\n")
find_inter_exon_markers(homologs, syns1, syns2, asse1, asse2, max_intron_length, min_primer_length)

########
# Quit #
########

exit()
