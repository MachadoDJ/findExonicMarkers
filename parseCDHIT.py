#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# parseCDHIT.py
# Script to parse results from CD-HIT-EST and CD-HIT-EST-2D

## Import modules and libraries
import re,sys,glob

## Arguments
parser=argparse.ArgumentParser()
parser.add_argument("--source1", help="Renamed exons from species 1", type=str, required=True)
parser.add_argument("--source2", help="Renamed exons from species 2", type=str, required=True)
parser.add_argument("--representatives1", help="Cluster file (.clstr) from CD-HIT-EST analyses of species 1", type=str, required=True)
parser.add_argument("--representatives2", help="Cluster file (.clstr) from CD-HIT-EST analyses of species 2", type=str, required=True)
parser.add_argument("--cluster1", help="Cluster file (.clstr) from CD-HIT-EST-2D, species 1 vs. species 2", type=str, required=True)
parser.add_argument("--cluster2", help="Cluster file (.clstr) from CD-HIT-EST-2D, species 2 vs. species 1", type=str, required=True)
args=parser.parse_args()


## Set global variables
source_fasta_1 = args.source1
source_fasta_2 = args.source2
representative_sequences_file_1 = args.representatives1
representative_sequences_file_2 = args.representatives2
cluster_file_1 = args.cluster1
cluster_file_2 = args.cluster2

## Define functions
def parse_renamed_fasta(file): # Read multifasta, return dic={description:sequence}
	dic={}
	handle=open(file,"rU")
	for line in handle.readlines():
			line=line.strip()
			if(not line):
				continue
			if(line.startswith(">")):
					description=re.compile(">(.+)").findall(line)[0]
					active_sequence_name=description
					if active_sequence_name not in dic:
						dic[active_sequence_name]=""
					continue
			dic[active_sequence_name]+=line
	handle.close()
	return dic

def parse_representative(file): # Read clstr file, return dic={cluster no.:sequence ids}
	dic={}
	handle=open(file,"rU")
	data=re.sub(">Cluster","#Cluster",handle.read())
	for cluster in re.compile("#(Cluster[^\n]+)\n([^#]+)",re.MULTILINE|re.DOTALL).findall(data):
		n=int(re.compile("(\d+)").findall(cluster[0].strip())[0])
		dic[n]=re.compile(">(.+?)\.\.\..").findall(cluster[1])
	handle.close()
	return dic

def parse_cluster(file): # Read clstr file, return dic={id from sp1:ids from sp2}
	dic={}
	handle=open(file,"rU")
	data=re.sub(">Cluster","#Cluster",handle.read())
	for cluster in re.compile("#(Cluster[^\n]+)\n([^#]+)",re.MULTILINE|re.DOTALL).findall(data):
		ids=re.compile(">(.+?)\.\.\..").findall(cluster[1])
		dic[ids[0]]=ids[1:]
	handle.close()
	return dic

def stats(source_fasta_dic_1,source_fasta_dic_2,representative_sequences_dic_1,representative_sequences_dic_2,cluster_dic_1,cluster_dic_2):
	sys.stdout.write("Sequences in fasta file 1          = {:,}\n".format(len(source_fasta_dic_1)))
	sys.stdout.write("Representative sequences in file 1 = {:,}\n".format(len(representative_sequences_dic_1)))
	sys.stdout.write("Clusters between file 1 and 2      = {:,}\n".format(len(cluster_dic_1)))
	sys.stdout.write("Sequences in fasta file 2          = {:,}\n".format(len(source_fasta_dic_2)))
	sys.stdout.write("Representative sequences in file 2 = {:,}\n".format(len(representative_sequences_dic_2)))
	sys.stdout.write("Clusters between file 2 and 1      = {:,}\n".format(len(cluster_dic_2)))
	return

def merge(representative_sequences_dic_1,representative_sequences_dic_2,cluster_dic_1,cluster_dic_2):
	number_of_clusters_left=[]
	sys.stdout.write("Merging clusters...\n")
	# Part 1: numbering representative sequences from file 1 [1 -> 1]
	set1={}
	set2={}
	n=0
	for cluster in representative_sequences_dic_1:
		for id in representative_sequences_dic_1[cluster]:
			set1[id]=n
		sys.stderr.write("Part 1 - n={:,}\n".format(n))
		n+=1
	number_of_clusters_left+=[len(set([set1[x] for x in set1]))]
	sys.stdout.write("...Part 1: {:,} clusters left\n".format(number_of_clusters_left[-1]))
	# Part 2: numbering sequences from file 2 that matches file 1 [1 -> 2]
	for id1 in set1:
		if(id1 in cluster_dic_1):
			for id2 in cluster_dic_1[id1]:
				set2[id2]=set1[id1]
				sys.stderr.write("Part 2 - ID 1={}, ID 2={}, n={}\n".format(id1,id2,set1[id1]))
	number_of_clusters_left+=[len(set([set1[x] for x in set1 ])|set([set2[x] for x in set2]))]
	sys.stdout.write("...Part 2: {:,} clusters left\n".format(number_of_clusters_left[-1]))
	# This inverted dictionaries will be used as a trick to save much time
	inv1={}
	for id in set1:
		val=set1[id]
		if(val in inv1):
			inv1[val]+=[id]
		else:
			inv1[val]=[id]
	inv2={}
	for id in set2:
		val=set2[id]
		if(val in inv2):
			inv2[val]+=[id]
		else:
			inv2[val]=[id]
	# Part 3: renumber sequences from file 1 and file 2 based on the cluster of representative sequences in file 2 [2 -> 2]
	for cluster in representative_sequences_dic_2:
		overlaps=[]
		for id in representative_sequences_dic_2[cluster]:
			if(id in set2):
				overlaps+=[set2[id]]
			else:
				overlaps+=[n]
				set2[id]=n
				if(n in inv2):
					inv2[n]+=[id]
				else:
					inv2[n]=[id]
		overlaps=list(set(overlaps)) # Each number on this list is a set of sequences; if there are different sets here, they will be merged
		sequences_to_renumber_1=[]
		sequences_to_renumber_2=[]
		for o in overlaps:
			if o in inv1:
				sequences_to_renumber_1+=inv1[o]
				del inv1[o]
			if o in inv2:
				sequences_to_renumber_2+=inv2[o]
				del inv2[o]
		sequences_to_renumber_1=list(set(sequences_to_renumber_1))
		inv1[n]=sequences_to_renumber_1
		sequences_to_renumber_2=list(set(sequences_to_renumber_2))
		inv2[n]=sequences_to_renumber_2
		for id in sequences_to_renumber_1:
			set1[id]=n
		for id in sequences_to_renumber_2:
			set2[id]=n
		sys.stderr.write("Part 3 - n={:,}\n".format(n))
		n+=1
	number_of_clusters_left+=[len(set([set1[x] for x in set1 ])|set([set2[x] for x in set2]))]
	sys.stdout.write("...Part 3: {:,} clusters left\n".format(number_of_clusters_left[-1]))
	# Clean up a little bit
	sequences_to_renumber_1=[]
	sequences_to_renumber_2=[]
	# Part 4: finish up using sequences from file 1 that matches file 2 [2 -> 1]
	overlaps={}
	m=n-1
	for id2 in cluster_dic_2:
		overlaps[id2]=[]
		if(id2 in set2):
			overlaps[id2]+=[set2[id2]]
		else:
			overlaps[id2]+=[n]
			set2[id2]=n
			if(n in inv2):
				inv2[n]+=[id2]
			else:
				inv2[n]=[id2]
		for id1 in cluster_dic_2[id2]:
			if(id1 in set1):
				overlaps[id2]+=[set1[id1]]
			else:
				overlaps[id2]+=[n]
				set1[id1]=n
				if(n in inv1):
					inv1[n]+=[id1]
				else:
					inv1[n]=[id1]
		sys.stderr.write("Part 4a - n={:,}\n".format(n))
		n+=1
	sys.stderr.write("Part 4a done!\n")
	list_1=[]
	for id2 in overlaps:
		new_group=set(overlaps[id2])
		if(not new_group in list_1):
			list_1+=[new_group]
	sys.stderr.write("\t[List 1: {:,}]\n".format(len(list_1)))
	list_2=[]
	while len(list_1)>=1:
		sys.stderr.write("\t[List 2: {:,}]\n".format(len(list_2)))
		x=list_1[0]
		list_1=list_1[1:]
		proceed=True
		for i in range(0,len(list_1)):
			y=list_1[i]
			if(x&y):
				xy=x|y
				list_1.remove(y)
				list_1.append(xy)
				proceed=False
				break
		if(proceed==True):
			if(not x in list_2):
				list_2.append(x)
	sys.stderr.write("\t[List 2: {:,}]\n".format(len(list_2)))
	for j in list_2:
		for i in j:
			if(i in inv1):
				if(n in inv1):
					inv1[n]+=inv1[i]
				else:
					inv1[n]=inv1[i]
				del inv1[i]
			else:
				pass
			if(i in inv2):
				if(n in inv2):
					inv2[n]+=inv2[i]
				else:
					inv2[n]=inv2[i]
				del inv2[i]
			else:
				pass
		sys.stderr.write("Part 4b - n={:,}\n".format(n))
		n+=1
	number_of_clusters_left+=[len(set([x for x in inv1 ])|set([x for x in inv2]))]
	sys.stdout.write("...Part 4: {:,} clusters left\n".format(number_of_clusters_left[-1]))
	return inv1,inv2

def export(source_fasta_dic_1,source_fasta_dic_2,inv1,inv2):
	most_representative_1=[]
	for cluster1 in sorted(inv1):
		longest_id=""
		longest_seq=""
		n=len(inv1[cluster1])
		for id1 in sorted(inv1[cluster1]):
			seq1=source_fasta_dic_1[id1]
			if(len(seq1)>len(longest_seq)):
				longest_id=id1
				longest_seq=seq1
		if(longest_seq):
			most_representative_1+=[">{} cluster_id={} size={}\n{}\n".format(longest_id,cluster1,n,longest_seq)]
		else:
			most_representative_1+=[">{} cluster_id={} size={}\n{}\n".format("NO_MATCH",cluster1,n,"NNN")]
	most_representative_2=[]
	for cluster2 in sorted(inv2):
		longest_id=""
		longest_seq=""
		n=len(inv2[cluster2])
		for id2 in sorted(inv2[cluster2]):
			seq2=source_fasta_dic_2[id2]
			if(len(seq2)>len(longest_seq)):
				longest_id=id2
				longest_seq=seq2
		if(longest_seq):
			most_representative_2+=[">{} cluster_id={} size={}\n{}\n".format(longest_id,cluster2,n,longest_seq)]
		else:
			most_representative_2+=[">{} cluster_id={} size={}\n{}\n".format("NO_MATCH",cluster2,n,"NNN")]
	handle=open("representatives_1.fasta","w")
	for entry in most_representative_1:
		handle.write(entry)
	handle.close()
	handle=open("representatives_2.fasta","w")
	for entry in most_representative_2:
		handle.write(entry)
	handle.close()
	sys.stdout.write("First longest sequences of each cluster were written into representatives_1.fasta and representatives_2.fasta\nAll done!\n")
	return

## Execute functions
representative_sequences_dic_1=parse_representative(representative_sequences_file_1) # dic={n:[seq]}
representative_sequences_dic_2=parse_representative(representative_sequences_file_2) # dic={n:[seq]}
cluster_dic_1=parse_cluster(cluster_file_1) # dic={seq1:[seq2]}
cluster_dic_2=parse_cluster(cluster_file_2) # dic={seq2:[seq1]}

# The following commented lines were used for testing and development
# source_fasta_dic_1={"a1":"GATACA","a2":"GATACA","a3":"GATACA","a4":"GATACA","a5":"GATACA","a6":"GATACA"}
# source_fasta_dic_2={"b1":"GATACA","b2":"GATACA","b3":"GATACA","b4":"GATACA","b5":"GATACA","b6":"GATACA","b7":"GATACA","b8":"GATACA","b9":"GATACA"}
# representative_sequences_dic_1={0:["a1","a2","a3"],1:["a5"],2:["a6"]}
# representative_sequences_dic_2={0:["b1","b2","b3"],1:["b4","b5"],2:["b6"],3:["b7"],4:["b8"],5:["b9"]}
# cluster_dic_1={"a1":["b1","b2","b3","b8"],"a2":["b1","b2"],"a3":["b1","b2","b3"],"a4":["b4","b5"],"a5":["b4","b5"],"a6":["b6"]}
# cluster_dic_2={"b1":["a2","a3"],"b2":["a1","a2","a3"],"b3":["a1","a2"],"b4":["a4","a5"],"b5":["a4","a5"],"b6":["a6"],"b8":["a1","a3"],"b9":["a1","a6"]}

inv1,inv2=merge(representative_sequences_dic_1,representative_sequences_dic_2,cluster_dic_1,cluster_dic_2) # dic={n:[seq]}
source_fasta_dic_1=parse_renamed_fasta(source_fasta_1) # dic={id:seq}
source_fasta_dic_2=parse_renamed_fasta(source_fasta_2) # dic={id:seq}
stats(source_fasta_dic_1,source_fasta_dic_2,representative_sequences_dic_1,representative_sequences_dic_2,cluster_dic_1,cluster_dic_2)
export(source_fasta_dic_1,source_fasta_dic_2,inv1,inv2)

## Quit
exit()
