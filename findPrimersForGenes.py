#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# findPrimersForGenes.py
# This program requires a working installation of Primer3 (tested with libprimer3 release 2.3.7)

################################
# Import modules and libraries #
################################

import argparse,re,sys,subprocess

#############
# Arguments #
#############

parser=argparse.ArgumentParser()
parser.add_argument("-c","--config",help="Path to directory with primer thermodynamic parameters",type=str,default="/Users/denis/Primer3/primer3-2.3.7/src/primer3_config/",required=False)
parser.add_argument("-o","--output",help="Output prefix",type=str,default="primers4genes",required=False)
parser.add_argument("-p","--primer3",help="Path to primer3_core executable",type=str,default="/Users/denis/Primer3/primer3-2.3.7/src/primer3_core",required=False)
parser.add_argument("-t","--table",help="Tab-separated table with sequence ID, sequence, target start, target end, and identity (.tsv file)",type=str,required=True)
args=parser.parse_args()

##################
# Main functions #
##################

def read_table():
	sys.stdout.write("> Parsing file {}\n".format(args.table))
	markers    = {}
	handle     = open(args.table,"r")
	line_count = 0
	for line in handle.readlines():
		line_count += 1
		line        = line.strip()
		line        = re.sub("\s*#.*","",line)
		if (line):
			try:
				tag, seq, sta, end, idy = line.split("\t")
			except:
				sys.stderr.write("¡ WARN !\n[ Something whent wrong while parsing file {}, line {} ]\n".format(args.table,line_count))
				pass
			else:
				if (tag in markers):
					sys.write.error("¡ WARN !\n[ Repeting sequence ID in file {}, line {} ]\n".format(args.table,line_count))
					pass
				else:
					markers[tag] = {"seq" : seq, "sta" : int(sta), "end" : int(end)}
	handle.close()
	sys.stdout.write("-- Found {} candidate markers\n".format(len(markers)))
	return markers

def runPrimer3(selected, primer3, config):
	sys.stdout.write("> Processing candidate markers\n")
	for tag in sorted(markers):
		seq    = markers[tag]["seq"]
		sta    = markers[tag]["sta"]
		end    = markers[tag]["end"]
		min    = end - sta # Minimum product size = target size plus two times min. primer size
		max    = len(seq) - 34 # Maximum product size = the entire sequence minus 34 (2 times 17, min. primer size)
		input  = """SEQUENCE_ID={}
SEQUENCE_TEMPLATE={}
SEQUENCE_TARGET={},{}
PRIMER_TASK=pick_sequencing_primers
PRIMER_PICK_LEFT_PRIMER=1
PRIMER_PICK_INTERNAL_OLIGO=0
PRIMER_PICK_RIGHT_PRIMER=1
PRIMER_NUM_RETURN=1
PRIMER_MIN_TM=57
PRIMER_OPT_TM=60
PRIMER_MAX_TM=75
PRIMER_PAIR_MAX_DIFF_TM=5
PRIMER_MIN_GC=20
PRIMER_OPT_GC=50
PRIMER_MAX_GC=80
PRIMER_GC_CLAMP=0
PRIMER_MAX_POLY_X=5
PRIMER_MIN_SIZE=17
PRIMER_OPT_SIZE=20
PRIMER_MAX_SIZE=30
PRIMER_MAX_NS_ACCEPTED=1
PRIMER_PRODUCT_SIZE_RANGE={}-{}
P3_FILE_FLAG=0
PRIMER_EXPLAIN_FLAG=1
PRIMER_LOWERCASE_MASKING=0
PRIMER_THERMODYNAMIC_PARAMETERS_PATH={}
=
""".format(tag, seq, sta, end - sta, min, max, config)
		handle = open("configuration.temp", "w")
		handle.write(input)
		handle.close()
		result = subprocess.run("{} < configuration.temp".format(primer3), shell=True, stdout = subprocess.PIPE).stdout.decode('utf-8')
		if ("ERROR" in result or "error" in result or "Error" in result):
			sys.stderr.write("¡ ERROR !\n[ Something went wrong while searching for primers in sequence {} ]\n// RESULTS :\n{}\n//\n".format(tag, result))
			exit()
		processResult(result)
		subprocess.run("rm configuration.temp", shell=True)
	sys.stdout.write("//\n")
	return

#######################
# Auxiliary functions #
#######################

def processResult(result):
	id = re.compile("SEQUENCE_ID=([^\s\n]+)").findall(result)[0]
	try:
		dna    = re.compile("SEQUENCE_TEMPLATE=([^\s\n]+)").findall(result)[0]
		p1     = re.compile("PRIMER_LEFT_0_SEQUENCE=([^\s\n]+)").findall(result)[0]
		p2     = re.compile("PRIMER_RIGHT_0_SEQUENCE=([^\s\n]+)").findall(result)[0]
		t1     = float(re.compile("PRIMER_LEFT_0_TM=([^\s\n]+)").findall(result)[0])
		t2     = float(re.compile("PRIMER_RIGHT_0_TM=([^\s\n]+)").findall(result)[0])
	except:
		sys.stderr.write("! WARN: {} failed.\n".format(id))
		pass
	else:
		sys.stdout.write("-- New markers found for candidate {}\n".format(id))
		sta1   = dna.find(p1)
		end1   = sta1 + len(p1)
		sta2   = dna.find(reverse_complement(p2))
		end2   = sta2 + len(p2)
		output = """>{0}
{1}
>{0}_L {2}:{3} TM={4}
{5}
>{0}_R {6}:{7} TM={8}
{9}
""".format(id, dna, sta1, end1, t1, p1, sta2, end2, t2, p2)
		handle = open("{}_{}.fasta".format(args.output, id),"w")
		handle.write(output)
		handle.close()
	return

def reverse_complement(seq):
	complement = {"A" : "T", "T" : "A", "U" : "A", "G" : "C", "C" : "G", "Y" : "R", "R" : "Y", "S" : "S", "W" : "W", "K" : "M", "M" : "K", "B" : "V", "D" : "H", "H" : "D", "V" : "B", "N" : "N", "a" : "t", "t" : "a", "u" : "a", "g" : "c", "c" : "g", "y" : "r", "r" : "y", "s" : "s", "w" : "w", "k" : "m", "m" : "k", "b" : "v", "d" : "h", "h" : "d", "v" : "b", "n" : "n"}
	bases      = list(seq)
	bases      = reversed([complement.get(base,base) for base in bases])
	bases      = ''.join(bases)
	return bases

#####################
# Execute functions #
#####################

markers = read_table()
runPrimer3(markers, args.primer3, args.config)
sys.stdout.write("> Done!\n")
exit() # Quit program
